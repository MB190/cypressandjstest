Feature: Login
  In order to use the app the user must be able to Login

  

  Scenario: 001- Verify ‘Worlds best App’ page components
    Given I have navigated to login page
    When the page has loaded
    Then I can see the page header has displayed
    And I can see a login title has displayed
    And I can see a email  field has displayed
    And  I can see a password field has displayed
    And I can see a login button has displayed



  Scenario: 002- Verify the login functionality result with valid login credentials

    Given I have navigated to login page
    And the page has loaded
    When I enter valid email into email field
    And  I enter valid password into password field
    And I click on login button
    Then I should log in and  see welcome massage appears
    

  Scenario Outline: 003- Verify the login functionality result with invalid login credentials

  Given I have navigated to login page
  And  the page has loaded
  When I enter invalid email into email field
  And  I enter invalid password into password field
  And I click on login button
  Then I should not login and see error massage appears

  


#   Scenario Outline: 004- Verify the login functionality result with invalid login credentials

#   Given I have navigated to login page
#   And  the page has loaded
#   When I enter invalid "<Email>" into email field
#   And  I enter invalid "<Password>" into password field
#   And I click on login button
#   Then I should not login and see error massage appears

# #     this will also  check with one of field blank and one of filed with valid credentials

#   Examples:
#   |Email                |Password   |
#   |test@drugdev.com     |           |
#   |                     |supers3cret|
#   |                     |           |
#   |te123@drugdev.com    |supers3cret|
#   |test@drugdev.com     |fine3cret  |
#   |automation@gmail.com |selenium   |

