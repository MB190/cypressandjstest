import { Given} from "cypress-cucumber-preprocessor/steps";
import { When ,Then} from "cypress-cucumber-preprocessor/steps"; 


/// <reference types="Cypress" />



const url = 'https://sprinkle-burn.glitch.me'


Given('i have navigated to login page', () => 

{
  cy.visit(url);
})


Then("i can see a page header has display", () => {
     cy.get("body.h100.sans-serif.bg-green:nth-child(2) header.header.bg-black-80.white > h1:nth-child(1)").should('be.visible');
});

Then("i can see login tital has display", () => 
{
  cy.get("body.h100.sans-serif.bg-green:nth-child(2) article.pa4.black-80.content form.shadow-4.mt2.bg-white.pa2 > h1.ma0.mb2").should('be.visible');
});

Then("i can see email filed has display", () => {

  cy.get("input[name='email']").should('be.visible');
    
});

Then("i can see password filed has display", () => {
  
  cy.get("input[name='password']").should('be.visible');
    
});

Then("i can see login button has display", () => 
{
  cy.get("body.h100.sans-serif.bg-green:nth-child(2) article.pa4.black-80.content form.shadow-4.mt2.bg-white.pa2 fieldset.bn div.flex:nth-child(4) > button.f5.dim.bn.ph3.pv2.mb2.dib.br1.white.bg-dark-green").
  should('be.visible');
});



when("i enter valid email into email field", () => {
  cy.get("input[name='email']").type("test@drugdev.com");
  
});

When("i enter valid password into password field", () => {
  cy.get("input[name='password']").type("supers3cret");
});

When("i click on login button", () => {
 cy.get("body.h100.sans-serif.bg-green:nth-child(2) article.pa4.black-80.content form.shadow-4.mt2.bg-white.pa2 fieldset.bn div.flex:nth-child(4) > button.f5.dim.bn.ph3.pv2.mb2.dib.br1.white.bg-dark-green").click();
  
});

Then("i shoud login and see welcome massage appears", () => {
  cy.get(".pa4 black-80 content").should('be.visible');
});

when("i enter invalid email into email field", () => {
  cy.get("input[name='email']").type("test123@drugdev.com");
  
});

When("i enter invalid password into password field", () => {
  cy.get("input[name='password']").type("jfdsupers3cret");
});

Then("i should not login and see error massage appears", () => {
  
  cy.get("#login-error-box").should('be.visible');

});